package org.carpenter.bill;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.regex.Pattern;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import com.novell.ldap.LDAPConstraints;
import com.novell.ldap.LDAPException;
import com.novell.ldap.LDAPModification;
import com.novell.ldap.LDAPSearchConstraints;
import net.sourceforge.myvd.chain.AddInterceptorChain;
import net.sourceforge.myvd.chain.BindInterceptorChain;
import net.sourceforge.myvd.chain.CompareInterceptorChain;
import net.sourceforge.myvd.chain.DeleteInterceptorChain;
import net.sourceforge.myvd.chain.ExetendedOperationInterceptorChain;
import net.sourceforge.myvd.chain.InterceptorChain;
import net.sourceforge.myvd.chain.ModifyInterceptorChain;
import net.sourceforge.myvd.chain.PostSearchCompleteInterceptorChain;
import net.sourceforge.myvd.chain.PostSearchEntryInterceptorChain;
import net.sourceforge.myvd.chain.RenameInterceptorChain;
import net.sourceforge.myvd.chain.SearchInterceptorChain;
import net.sourceforge.myvd.core.NameSpace;
import net.sourceforge.myvd.inserts.Insert;
import net.sourceforge.myvd.types.Attribute;
import net.sourceforge.myvd.types.Bool;
import net.sourceforge.myvd.types.DistinguishedName;
import net.sourceforge.myvd.types.Entry;
import net.sourceforge.myvd.types.ExtendedOperation;
import net.sourceforge.myvd.types.Filter;
import net.sourceforge.myvd.types.Int;
import net.sourceforge.myvd.types.Password;
import net.sourceforge.myvd.types.Results;

/**
 * This class adds an external callout to each normal method. Configuration provides
 * a class name, and static or instance methods on that class have the same names as Insert methods.
 * If any of the methods are not static, there must be an accessible no-arg constuctor for
 * creating an instance of the class. That instance will be reused when any of those non-static
 * methods is called. An optional initializer method can be configured, and the initializer
 * can be a static or instance method. If it is static, it will still be called once for each
 * configured use of ExternalCalloutInsert that specifies that class. If the list of init
 * arguments is not the same, your callout class will have to understand that.
 *
 * Why would you want to use this insert? It does allow you to make calls to
 * libraries that don't know anything about MyVD, but such a library is unlikely
 * to have methods with the applicable signatures (see below for details). That means
 * you probably will still have to do at least a small piece of coding to match
 * things up. So, why not just do a custom insert that does exactly what you want?
 * Maybe you want to keep the other library completely decoupled from MyVD work,
 * including avoiding the small bother of having the MyVD dependencies in the
 * development environment for that library. Or maybe you just prefer to be using
 * all generic inserts in your MyVD configuration. Anyhow, the choice is yours,
 * and you can use this if you find it convenient.
 *
 * Each method returns a boolean, and the parameters for each method consist of:
 *
 * -- The bind user identity (as a string)
 * -- An array of strings taken from configuration
 * -- An object array of most of the parameters with which the Insert method was called
 *    (the chain parameter is not passed)
 *
 * Thus the signature of each such method is:
 *
 * boolean someMethod(String, String[], Object[])
 *
 * If a method of the appropriate name does not exist or is not accessible, it's skipped, and the "next"
 * method for the chain is called. If the method returns true, then the "next" method
 * in the chain is called. If the method returns false or throws an exception,
 * the "next" method in the chain is not called.
 *
 * Callouts are not applied to Insert.configure(), Insert.getName(), or Insert.shutdown().
 * When the callout is made for the Insert.bind() method, the user will not yet have been
 * authenticated.
 *
 * The following properties are used:
 *
 * className: the fully-qualified Java classname of the callout class.
 *
 * arg.0 ... arg.N: the items for the String[] to be passed to callout methods; each
 * call to a callout method gets its own copy of the array, so there is no point
 * in the method modifying array elements. Do not leave gaps in the numbering.
 *
 * dnregex - A Java regular expression (java.util.regex.Pattern) that matches against
 * the entire DN string. You will typically use capture groups in that regex to
 * pick the pieces you want out of the DN string. Since the entire DN string is
 * matched, it is fairly typical to actually use capture group 1.
 *
 * template - (default: {$1}) The user string for authentications is based on this template.
 * It can and typically will have items that can be replaced by things matched by the regex.
 * Substrings of the form "{$1}" (no quotes) will be replaced by the corresponding regex
 * capture group. You can have any number of those substrings in the template, with
 * numbers up to the number of capture groups from the regex. That includes the
 * possible use of "{$0}" with the usual meaning.
 *
 * init.methodName - A static or instance method to be called at MyVD configure() time.
 * If not set, no call is made. Must be accessible. It takes two arguments. First, a
 * String giving the MyVD name for the ExternalCalloutInsert instance. Second, a String[]
 * giving the configured init arguments.
 *
 * init.0 ... init.N: the items for the String[] to be passed to the initialization method; each
 * call to the initialization method gets its own copy of the array, so there is no point
 * in the method modifying array elements. Do not leave gaps in the numbering.
 *
 * This insert has an external dependency on the callout class, which must be found
 * along the classpath. Typically, that class will be in a JAR that you drop into
 * the MyVD lib/ directory.
 */
public class ExternalCalloutInsert implements Insert {
  private Logger logger = LogManager.getLogger(ExternalCalloutInsert.class);
  private String name;
  private Pattern dnRegex;
  private String template;
  private Class<?> calloutClass;
  private Object calloutInstance;
  private String[] calloutArgs;

  private String isolateUserFromBindUser(DistinguishedName bindDN, InterceptorChain chain) {
    if (bindDN == null) {
      bindDN = (DistinguishedName) chain.getSession().get("MYVD_BINDDN");
    }
    if (bindDN == null) {
      throw new IllegalStateException("There is no authenticated user for " + this.getClass().getSimpleName());
    }
    logger.debug("DN '" + bindDN + "'");
    final String user = Util.matchAndReplace(dnRegex, bindDN.toString(), template);
    return user;
  }

  private boolean invokeCallout(InterceptorChain chain, DistinguishedName dn, String methodName, Object[] originalArgs) {
    Method calloutMethod;
    try {
      // never returns null; throws if it can't find a match
      calloutMethod = calloutClass.getMethod(methodName, String.class, String[].class, Object[].class);
    } catch (final Throwable t) {
      if (!(t instanceof NoSuchMethodException)) {
        // NoSuchMethodException normal and expected in the majority of cases; ignore
        logger.catching(Level.INFO, t);
      }
      return true;
    }
    final String user = isolateUserFromBindUser(dn, chain);
    logger.debug("Invoking callout method " + calloutClass.getName() + "." + methodName + "() for user " + user);
    try {
      return (boolean)calloutMethod.invoke(calloutInstance, user, calloutArgs, originalArgs);
    } catch (final Throwable t) {
      logger.catching(Level.INFO, t);
      logger.info("Callout method " + calloutClass.getName() + "." + methodName + "() threw an exception: " + t);
      Throwable causedBy = t.getCause();
      while (causedBy != null) {
        logger.info("Callout method " + calloutClass.getName() + "." + methodName + "()          Caused by: " + causedBy);
        causedBy = causedBy.getCause();
      }
      return false;
    }
  }


  @Override
  public String getName() {
    return name;
  }

  @Override
  public void configure(String name, Properties props, NameSpace ns) throws LDAPException {
    this.name = name;
    logger.info("name: " + name);
    logger = LogManager.getLogger(logger.getName() + ":" + name);
    final String className = props.getProperty("className");
    logger.info("className: " + className);
    setupCalloutClass(className);
    this.dnRegex = Pattern.compile(props.getProperty("dnregex"));
    logger.info("dnregex: " + dnRegex);
    this.template = props.getProperty("template", "{$1}");
    logger.info("template: " + template);
    final List<String> argsList = new ArrayList<String>();
    for (int adex=0; ; ++adex) {
      final String arg = props.getProperty("arg." + adex);
      if (arg == null) break;
      argsList.add(arg);
    }
    logger.info("args: " + argsList);
    calloutArgs = argsList.toArray(new String[0]);

    // optional static initialization; method(String[])
    final String initMethodName = props.getProperty("init.methodName");
    logger.info("init.methodName: " + initMethodName);
    if (initMethodName != null) {
      callCalloutInit(props, className, initMethodName);
    }
  }

  private void setupCalloutClass(final String className) {
    try {
      calloutClass = Class.forName(className);
      final Constructor<?> noargs = calloutClass.getConstructor(new Class[0]);
      if (noargs != null) {
        calloutInstance = noargs.newInstance((Object[])null);
      } else {
        logger.info("Callout class '" + className
            + "' doesn't have an accessible no-arg constructor. All methods must be static.");
        calloutInstance = null;
      }
    } catch (final ClassNotFoundException | SecurityException e) {
      logger.error(e);
      throw new IllegalArgumentException("Callout class '" + className
          + "' could not be loaded", e);
    } catch (final NoSuchMethodException e) {
      logger.info("Callout class '" + className
          + "' doesn't have an accessible no-arg constructor. All methods must be static.");
      calloutInstance = null;
    } catch (InstantiationException | IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
      Throwable t = e;
      if (e instanceof InvocationTargetException) {
        t = e.getCause();
      }
      logger.error(t);
      throw new IllegalArgumentException("Could not instantiate Callout class '" + className
          + "' could not be loaded", t);
    }
  }

  private void callCalloutInit(Properties props, final String className,
      final String initMethodName) {
    Method initMethod;
    try {
      initMethod = calloutClass.getMethod(initMethodName, String.class, String[].class);
    } catch (NoSuchMethodException | SecurityException e) {
      logger.error(e);
      throw new IllegalArgumentException(className + "." + initMethodName + "(String[]) could not be found", e);
    }
    final List<String> initList = new ArrayList<String>();
    for (int idex=0; ; ++idex) {
      final String init = props.getProperty("init." + idex);
      if (init == null) break;
      initList.add(init);
    }
    logger.info("inits: " + initList);
    final String[] calloutInits = initList.toArray(new String[0]);
    try {
      initMethod.invoke(calloutInstance, getName(), calloutInits);
    } catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
      Throwable t = e;
      if (e instanceof InvocationTargetException) {
        t = e.getCause();
      }
      logger.error(t);
      throw new IllegalArgumentException(className + "." + initMethodName + "(String[]) threw an exception when invoked", t);
    }
  }

  @Override
  public void bind(BindInterceptorChain chain, DistinguishedName userDn, Password password, LDAPConstraints constraints) throws LDAPException {
    if (invokeCallout(chain, userDn, "bind", new Object[] {userDn, password, constraints})) {
      chain.nextBind(userDn, password, constraints);
    }
  }

  @Override
  public void add(AddInterceptorChain chain, Entry entry, LDAPConstraints constraints) throws LDAPException {
    if (invokeCallout(chain, null, "add", new Object[] {entry, constraints})) {
      chain.nextAdd(entry, constraints);
    }
  }

  @Override
  public void compare(CompareInterceptorChain chain, DistinguishedName base, Attribute attribute, LDAPConstraints constraints) throws LDAPException {
    if (invokeCallout(chain, null, "compare", new Object[] {base, attribute, constraints})) {
      chain.nextCompare(base, attribute, constraints);
    }
  }

  @Override
  public void delete(DeleteInterceptorChain chain, DistinguishedName base, LDAPConstraints constraints) throws LDAPException {
    if (invokeCallout(chain, null, "delete", new Object[] {base, constraints})) {
      chain.nextDelete(base, constraints);
    }
  }

  @Override
  public void extendedOperation(ExetendedOperationInterceptorChain chain, ExtendedOperation extOp, LDAPConstraints constraints) throws LDAPException {
    if (invokeCallout(chain, null, "extendedOperation", new Object[] {extOp, constraints})) {
      chain.nextExtendedOperations(extOp, constraints);
    }
  }

  @Override
  public void modify(ModifyInterceptorChain chain, DistinguishedName base, ArrayList<LDAPModification> modifications, LDAPConstraints constraints) throws LDAPException {
    if (invokeCallout(chain, null, "modify", new Object[] {base, modifications, constraints})) {
      chain.nextModify(base, modifications, constraints);
    }
  }

  @Override
  public void postSearchComplete(PostSearchCompleteInterceptorChain chain, DistinguishedName base, Int scope, Filter filter, ArrayList<Attribute> attributes, Bool typesOnly, LDAPSearchConstraints constraints) throws LDAPException {
    if (invokeCallout(chain, null, "postSearchComplete", new Object[] {base, scope, filter, attributes, typesOnly, constraints})) {
      chain.nextPostSearchComplete(base, scope, filter, attributes, typesOnly, constraints);
    }
  }

  @Override
  public void postSearchEntry(PostSearchEntryInterceptorChain chain, Entry entry, DistinguishedName base, Int scope, Filter filter, ArrayList<Attribute> attributes, Bool typesOnly, LDAPSearchConstraints constraints) throws LDAPException {
    if (invokeCallout(chain, null, "postSearchEntry", new Object[] {entry, base, scope, filter, attributes, typesOnly, constraints})) {
      chain.nextPostSearchEntry(entry, base, scope, filter, attributes, typesOnly, constraints);
    }
  }

  @Override
  public void rename(RenameInterceptorChain chain, DistinguishedName dn, DistinguishedName newRdn, Bool deleteOldRdn, LDAPConstraints constraints) throws LDAPException {
    if (invokeCallout(chain, null, "rename", new Object[] {dn, newRdn, deleteOldRdn, constraints})) {
      chain.nextRename(dn, newRdn, deleteOldRdn, constraints);
    }
  }

  @Override
  public void rename(RenameInterceptorChain chain, DistinguishedName dn, DistinguishedName newRdn, DistinguishedName newParentDN, Bool deleteOldRdn, LDAPConstraints constraints) throws LDAPException {
    if (invokeCallout(chain, null, "rename", new Object[] {dn, newRdn, newParentDN, deleteOldRdn, constraints})) {
      chain.nextRename(dn, newRdn, newParentDN, deleteOldRdn, constraints);
    }
  }

  @Override
  public void search(SearchInterceptorChain chain, DistinguishedName base, Int scope, Filter filter, ArrayList<Attribute> attributes, Bool typesOnly, Results results, LDAPSearchConstraints constraints) throws LDAPException {
    if (invokeCallout(chain, null, "search", new Object[] {base, scope, filter, attributes, typesOnly, results, constraints})) {
      chain.nextSearch(base, scope, filter, attributes, typesOnly, results, constraints);
    }
  }

  @Override
  public void shutdown() {
  }
  /**
   * You can use this class to test callouts. It has bind and search methods which always return true.
   * If you reference this class in your MyVD config, remember to use ...ExternalCalloutInsert$SampleCallout,
   * since that's how Java names nested classes.
   */
  public static class SampleCallout {
    private final static Logger logger = LogManager.getLogger(SampleCallout.class);
    public SampleCallout() {}  // must be public if any methods are non-static
    public void sampleInstanceInit(String name, String[] inits) {
      SampleCallout.dumpInits(name + " sampleInstanceInit", inits);
    }
    public static void sampleStaticInit(String name, String[] inits) {
      SampleCallout.dumpInits(name + " sampleStaticInit", inits);
    }
    public static boolean search(String user, String[] calloutArgs, Object[] originalArgs) {
      SampleCallout.logger.info("static " + SampleCallout.class.getName() + ".search() invoked for user '" + user + "'");
      SampleCallout.dumpArgs(calloutArgs, originalArgs);
      return true;
    }
    public boolean bind(String user, String[] calloutArgs, Object[] originalArgs) {
      SampleCallout.logger.info("instance " + SampleCallout.class.getName() + ".bind() invoked for user '" + user + "'");
      SampleCallout.dumpArgs(calloutArgs, originalArgs);
      return true;
    }
    private static void dumpArgs(String[] calloutArgs, Object[] originalArgs) {
      if (calloutArgs == null) {
        calloutArgs = new String[0];
      }
      if (originalArgs == null) {
        originalArgs = new Object[0];
      }
      SampleCallout.logger.info("Callout args String[" + calloutArgs.length + "]:");
      for (int ii=0; ii<calloutArgs.length; ++ii) {
        String arg = calloutArgs[ii];
        if (arg != null) {
          arg = "\"" + arg + "\"";
        }
        SampleCallout.logger.info("  args[" + ii + "]: " + arg);
      }
      SampleCallout.logger.info("Orignal arg types Object[" + originalArgs.length + "]:");
      for (int ii=0; ii<originalArgs.length; ++ii) {
        final Object arg = originalArgs[ii];
        String type = null;
        if (arg != null) {
          type = arg.getClass().getName();
        }
        SampleCallout.logger.info("  args[" + ii + "] type: " + type);
      }
    }
    private static void dumpInits(String methodName, String[] inits) {
      if (inits == null) {
        inits = new String[0];
      }
      SampleCallout.logger.info("Init method " + methodName + "(String[" + inits.length + "]):");
      for (int ii=0; ii<inits.length; ++ii) {
        String arg = inits[ii];
        if (arg != null) {
          arg = "\"" + arg + "\"";
        }
        SampleCallout.logger.info("  args[" + ii + "]: " + arg);
      }
    }
  }
}
