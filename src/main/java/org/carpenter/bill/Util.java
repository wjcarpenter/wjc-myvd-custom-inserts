package org.carpenter.bill;

import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import edu.emory.mathcs.backport.java.util.Arrays;

public class Util {
  private static Logger logger = LogManager.getLogger(Util.class);
  private Util() { /* static only */}

  // backslash has to come first
  private static final String[] snames =   {  "\\  ",  "NUL",    "(  ",    ")  ",    "*  "}; //, "@  ", "^  "};
  private static final String[] specials = {    "\\",   "\0",      "(",      ")",      "*"}; //for debugging,   "@",   "^"};
  private static final String[] escapes  = {  "\\5c", "\\00",   "\\28",   "\\29",   "\\2a"}; //,   "^",   "@"};

  /**
   * This method applies LDAP special character escape sequences to the input
   * string and returns the escaped string. The LDAP special characters are
   * null, open and close parentheses, asterisk, and backslash. The escape
   * sequences that replace them consist of a backslash and 2 hex digits.
   */
  public static String LDAPescaped(String s) {
    for (int ii=0; ii<Util.specials.length; ++ii) {
      final String special = Util.specials[ii];
      final String escape  = Util.escapes[ii];
      final String t = s.replace(special, escape);
      if (Util.logger.isDebugEnabled()) {
        final String sname = Util.snames[ii];
        if (t.equals(s)) {
          Util.logger.debug("For " + sname + ", NO   replacements in '" + s + "'");
        } else {
          Util.logger.debug("For " + sname + ", SOME replacements in '" + s + "', giving '" + t + "'");
        }
      }
      s = t;
    }
    return s;
  }

  /**
   * Special case convenience method for just one template.
   */
  static String matchAndReplace(Pattern pattern, String subject, String template) {
    final String[] results = Util.matchAndReplace(pattern, subject, new String[] {template});
    return results[0];
  }

  /**
   * Matches a pattern against a subject string. It must match. Then each template
   * is expanded by replacing "{$1}", "{$2}", etc, with the contents of the regex
   * capture groups.
   */
  static String[] matchAndReplace(Pattern pattern, String subject, String[] templates) {
    Util.logger.debug("Subject '" + subject + "'");
    final Matcher matcher = pattern.matcher(subject);
    if (!matcher.matches()) {
      throw new IllegalStateException("No match for regex for insert." );
    }
    final String[] results = new String[templates.length];
    for (int ii=0; ii<templates.length; ++ii) {
      results[ii] = Util.expandOneTemplate(matcher, templates[ii]);
    }
    Util.logger.debug("results " + Arrays.toString(results));
    return results;
  }

  private static String expandOneTemplate(final Matcher matcher, String template) {
    final StringBuilder userString = new StringBuilder(template);
    for (int mdex=0; mdex<=matcher.groupCount(); ++mdex) {
      final String replacement = matcher.group(mdex);
      if (replacement == null) continue;
      final String placeHolder = "{$" + mdex + "}";
      while (true) {
        final int foundIt = userString.indexOf(placeHolder);
        if (foundIt < 0) break;
        Util.logger.debug("REPLACE '" + placeHolder + "' with '" + replacement + "' in '" + userString + "'");
        userString.replace(foundIt, foundIt + placeHolder.length(), replacement);
        Util.logger.debug("RESULT '" + userString + "'");
      }
    }
    return userString.toString();
  }


}
