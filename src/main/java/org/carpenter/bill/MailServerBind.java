package org.carpenter.bill;

import java.nio.charset.StandardCharsets;
import java.util.Properties;
import java.util.regex.Pattern;
import javax.mail.AuthenticationFailedException;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Store;
import javax.mail.Transport;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import com.novell.ldap.LDAPConstraints;
import com.novell.ldap.LDAPException;
import net.sourceforge.myvd.chain.BindInterceptorChain;
import net.sourceforge.myvd.core.NameSpace;
import net.sourceforge.myvd.inserts.Insert;
import net.sourceforge.myvd.types.DistinguishedName;
import net.sourceforge.myvd.types.Password;

/**
 * This class provides a bind() implementation that uses JavaMail
 * to do the authentication against a POP3/IMAP4/SMTP server. After
 * an attempted authentication, the insert disconnects from that server.
 * If authentication fails, the insert throws LDAPException. If authentication
 * succeeds, the chainNext is called in case you need to chain together
 * multiple successful bind() calls.
 *
 * The following properties are used:
 *
 * server (default: localhost) - The name of the server to connect to for authentication.
 *
 * port (default: -1) - The TCP port to use for authentication. -1 means to use the
 * default port for the protocol.
 *
 * protocol (default: pop3) - The protocol to use. The possibilities are "pop3",
 * "imap", and "smtp"
 *
 * starttls (default: false) - Whether or not to use StartTLS.
 *
 * dnregex - A Java regular expression (java.util.regex.Pattern) that matches against
 * the entire DN string. You will typically use capture groups in that regex to
 * pick the pieces you want out of the DN string. Since the entire DN string is
 * matched, it is fairly typical to actually use capture group 1.
 *
 * template - (default: {$1})The user string for authentications is based on this template.
 * It can and typically will have items that can be replaced by things matched by the regex.
 * Substrings of the form "{$1}" (no quotes) will be replaced by the corresponding regex
 * capture group. You can have any number of those substrings in the template, with
 * numbers up to the number of capture groups from the regex. That includes the
 * possible use of "{$0}" with the usual meaning.
 *
 * This insert has an external dependency on JavaMail. If you just follow a maven
 * dependency on JavaMail, you might only get the exposed API. You need to full
 * JAR that includes the implementation. Find it at https://javaee.github.io/javamail/
 */
public class MailServerBind extends TrivialBaseInsert implements Insert {
  private Logger logger = LogManager.getLogger(MailServerBind.class);
  private String name;
  private String server;
  private int port;
  private String protocol;
  private boolean starttls;
  private Pattern dnRegex;
  private String template;

  @Override
  public String getName() {
    return name;
  }

  @Override
  public void configure(String name, Properties props, NameSpace ns) throws LDAPException {
    this.name = name;
    logger.info("name: " + name);
    logger = LogManager.getLogger(logger.getName() + ":" + name);
    this.server = props.getProperty("server", "localhost");
    logger.info("server: " + server);
    this.port = Integer.parseInt(props.getProperty("port", "-1"));
    logger.info("port: " + port);
    this.protocol = props.getProperty("protocol", "pop3");
    logger.info("protocol: " + protocol);
    this.starttls = Boolean.parseBoolean(props.getProperty("starttls", "false"));
    logger.info("starttls: " + starttls);
    this.dnRegex = Pattern.compile(props.getProperty("dnregex"));
    logger.info("dnregex: " + dnRegex);
    this.template = props.getProperty("template", "{$1}");
    logger.info("template: " + template);
  }

  @Override
  public void bind(BindInterceptorChain chain, DistinguishedName userDn, Password password, LDAPConstraints constraints) throws LDAPException {
    logger.debug(this.getClass().getSimpleName() + ".bind() called for bind DN=" + userDn);
    final String user = Util.matchAndReplace(dnRegex, userDn.toString(), template);
    String pass;
    pass = new String(password.getValue(), StandardCharsets.UTF_8);
    logger.debug(this.getClass().getSimpleName() + ".bind() authenticating user '" + user + "'");
    final Properties props = new Properties();
    props.put("mail." + protocol + ".auth", "true");
    props.put("mail." + protocol + ".starttls.enable", (starttls ? "true" : "false"));
    final Session session = Session.getDefaultInstance(props);
    try {
      if (protocol.equalsIgnoreCase("smtp")) {
        try (Transport transport = session.getTransport(protocol)) {
          transport.connect(server, port, user, pass);
        } catch (final AuthenticationFailedException e) {
          logger.debug("Authentication failed: " + e);
          throw new LDAPException("Could not execute bind",LDAPException.OPERATIONS_ERROR,LDAPException.resultCodeToString(LDAPException.OPERATIONS_ERROR), e);
        }
      } else {
        try (Store store = session.getStore(protocol)) {
          store.connect(server, port, user, pass);
        } catch (final AuthenticationFailedException e) {
          logger.debug("Authentication failed: " + e);
          throw new LDAPException("Could not execute bind",LDAPException.OPERATIONS_ERROR,LDAPException.resultCodeToString(LDAPException.OPERATIONS_ERROR), e);
        }
      }
    } catch (final MessagingException e) {
      throw new LDAPException("Could not execute bind",LDAPException.OPERATIONS_ERROR,LDAPException.resultCodeToString(LDAPException.OPERATIONS_ERROR), e);
    }
    logger.debug("Authentication step passed for user " + userDn);
    chain.nextBind(userDn, password, constraints);
    return;
  }
}
