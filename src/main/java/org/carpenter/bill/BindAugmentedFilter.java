package org.carpenter.bill;

import java.util.ArrayList;
import java.util.Properties;
import java.util.regex.Pattern;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import com.novell.ldap.LDAPException;
import com.novell.ldap.LDAPSearchConstraints;
import net.sourceforge.myvd.chain.InterceptorChain;
import net.sourceforge.myvd.chain.SearchInterceptorChain;
import net.sourceforge.myvd.core.NameSpace;
import net.sourceforge.myvd.inserts.Insert;
import net.sourceforge.myvd.types.Attribute;
import net.sourceforge.myvd.types.Bool;
import net.sourceforge.myvd.types.DistinguishedName;
import net.sourceforge.myvd.types.Filter;
import net.sourceforge.myvd.types.Int;
import net.sourceforge.myvd.types.Results;

/**
 * This insert gives you the ability to augment the search filter with additional
 * things from the bind user DN. One reason for using this is if you have data
 * in a database that is somehow partitioned by calling user. Adding matching on
 * that user to the Filter will cause JdbcInsert to add that matching condition
 * to the SQL query. That's good for performance and also for preventing other
 * users' data from being returned in search results. It's only used for search,
 * so don't count on it for enforcing any security for other operations.
 *
 * Two properties control the behavior. Set these properties in the MyVD config
 * file.
 *
 * dnregex - A Java regular expression (java.util.regex.Pattern) that matches against
 * the entire DN string. You will typically use capture groups in that regex to
 * pick the pieces you want out of the DN string. Since the entire DN string is
 * matched, it is fairly typical to actually use capture group 1.
 *
 * template - The new Filter string is based on this template. It can and typically
 * will have items that can be replaced by things matched by the regex. Substrings
 * of the form "{$1}" (no quotes) will be replaced by the corresponding regex
 * capture group. You can have any number of those substrings in the template, with
 * numbers up to the number of capture groups from the regex. That includes the
 * possible use of "{$0}" with the usual meaning. After capture group replacement
 * processing, any occurrences of the literal string "{$F}" (no quotes) are replaced
 * with the original Filter string.
 *
 * For pieces of the DN matched and replaced into the template, LDAP escaping is done.
 *
 * This insert has no external dependencies.
 */
public class BindAugmentedFilter extends TrivialBaseInsert implements Insert {
  private Logger logger = LogManager.getLogger(BindAugmentedFilter.class);
  private String name;
  private Pattern dnregex;
  private String template;

  @Override
  public String getName() {
    return name;
  }

  @Override
  public void configure(String name, Properties props, NameSpace ns) throws LDAPException {
    this.name = name;
    logger.info("name: " + name);
    logger = LogManager.getLogger(logger.getName() + ":" + name);
    final String regexString = props.getProperty("dnregex");
    if (regexString == null) {
      throw new IllegalArgumentException("no value provided for dnregex property");
    }
    dnregex = Pattern.compile(regexString);
    logger.info("dnregex: " + dnregex);
    template = props.getProperty("template", "(&(owner={$1}){$F})");
    logger.info("template: " + template);
  }

  @Override
  public void search(SearchInterceptorChain chain, DistinguishedName base, Int scope, Filter filter, ArrayList<Attribute> attributes, Bool typesOnly, Results results, LDAPSearchConstraints constraints) throws LDAPException {
    final Filter augmentedFilter = augmentFilterWithBindUser(filter, chain);
    chain.nextSearch(base, scope, augmentedFilter, attributes, typesOnly, results, constraints);
  }

  private Filter augmentFilterWithBindUser(Filter originalFilter, InterceptorChain chain) {
    // The easy way out, not the elegant way. Guaranteed to leave the original filter unaffected.
    //
    // The received filter will typically look something like this.
    //
    //  (&(objectclass=inetOrgPerson)(|(cn=xyz*)(mail=xyz*)(sn=xyz*)))

    final DistinguishedName bindDN = (DistinguishedName) chain.getSession().get("MYVD_BINDDN");
    final String userInDn = bindDN.toString();
    logger.debug("DN '" + userInDn + "'");
    final StringBuilder newFilterString = new StringBuilder(Util.matchAndReplace(dnregex, userInDn, template));

    while (true) {
      final String orignalFilterPlaceHolder = "{$F}";
      final int foundIt = newFilterString.indexOf(orignalFilterPlaceHolder);
      if (foundIt < 0) break;
      final String orignalFilterString = originalFilter.getValue();
      logger.debug("REPLACE '" + orignalFilterPlaceHolder + "' with '" + orignalFilterString + "' in '" + newFilterString + "'");
      newFilterString.replace(foundIt, foundIt + orignalFilterPlaceHolder.length(), orignalFilterString);
      logger.debug("RESULT '" + newFilterString + "'");
    }

    try {
      return new Filter(newFilterString.toString());
    } catch (final LDAPException e) {
      throw new IllegalStateException("Unable to create the augmented Filter", e);
    }
  }
}
