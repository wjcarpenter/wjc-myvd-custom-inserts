package org.carpenter.bill;

import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;
import java.util.regex.Pattern;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import com.novell.ldap.LDAPAttribute;
import com.novell.ldap.LDAPEntry;
import com.novell.ldap.LDAPException;
import com.novell.ldap.LDAPSearchConstraints;
import net.sourceforge.myvd.chain.PostSearchEntryInterceptorChain;
import net.sourceforge.myvd.core.NameSpace;
import net.sourceforge.myvd.inserts.Insert;
import net.sourceforge.myvd.types.Attribute;
import net.sourceforge.myvd.types.Bool;
import net.sourceforge.myvd.types.DistinguishedName;
import net.sourceforge.myvd.types.Entry;
import net.sourceforge.myvd.types.Filter;
import net.sourceforge.myvd.types.Int;

/**
 * This insert provides a way to sort the values of a multi-valued attribute.
 * In general, LDAP attributes don't have a particular ordering, but clients
 * may make assumptions about ordering. For example, some faulty clients
 * will only consider the first value in a multi-valued attribute and ignore
 * the other values. Knowing that, if there were a most preferred value among
 * the multiple values, you could arrange for it to be first in the list.
 *
 * The order that you want might not be plain, old lexical order. If you
 * arrange for your attribute values to have a key part and a "real" value
 * part, this insert will parse the keys and values out of the incoming
 * attribute values. A regular expressions with matching groups is used to
 * find them. The defaults are arranged such that the entire attribute value
 * is used as both the key and the "real" value.
 *
 * attributeName (no default): The name of the attribute whose values will be
 * modified. This property is required in the configuration. However, if
 * no attribute of that name is present in LDAP entry, the insert silently
 * returns with no changes. Case-insensitive.
 *
 * renameTo (optional, no default): If configured, the attribute will be renamed
 * to this value. Case-insensitive. If there is already an attribute with this
 * name, it will be replaced. This is important for any attributes that might
 * appear in an LDAP filter since MyVD will be filtering on the attribute values,
 * and you will have modified those values to inject the sort key. Thus, you
 * want to define attribute "A" with the normal value and "AX" for the value
 * augmented with the sort key. Then, rename attribute "AX" to "A" so that the
 * sorted values replace the unsorted values in the original "A" attribute.
 *
 * charsetIn (optional, default ISO-8859-1): Attribute values are carried internally and
 * given to this insert as byte arrays. This insert converts them to strings
 * using this character set for decoding. The character set name must be one
 * recognized by Java Charset.forName().
 *
 * charsetOut (optional, default ISO-8859-1): Likewise, this character set is used when
 * converting the sorted string values back to byte arrays after the sorting
 * process.
 *
 * attributeRegex (optional, default is "^.*$" [the entire value]): The
 * stringified attribute value is matched against this regular expression
 * for the purposes of finding the key and final value. It's not strictly
 * necessary to match the entire value string, but the defaults for the
 * key and value templates are the entirety of what the regex does match.
 * And the regex _must_ match against each value, or an exception will be
 * thrown.
 *
 * keyTemplate (optional, default is "{$0}" [the entire regex match]): After matching
 * with "attributeRegex", this template is used to create the sorting key.
 * It can have items that can be replaced by things matched by the regex. Substrings
 * of the form "{$1}" (no quotes) will be replaced by the corresponding regex
 * capture group. You can have any number of those substrings in the template, with
 * numbers up to the number of capture groups from the regex. That includes the
 * possible use of "{$0}" with the usual meaning.
 *
 * valueTemplate (optional, default is "{$0}" [the entire regex match]): After matching
 * with "attributeRegex", this template is used to create the final attribute value.
 * See the description of "keyTemplate".
 *
 * keyType (optional, default is "string"): How the key should be interpreted for
 * sorting purposes. Must be one of "string", "integer", "float", or "boolean".
 * The key type names are case-insensitive.
 *
 * sortDirection (optional, default is "ascending"): The direction for the
 * sorting of the keys. Must be one of "ascending" or "descending". The sort
 * direction names are case-insensitive.
 *
 * This insert has no external dependencies.
 */
public class AttributeSortingInsert extends TrivialBaseInsert implements Insert {
  private Logger logger = LogManager.getLogger(AttributeSortingInsert.class);
  private String name;
  private String attributeName;
  private String renameTo;
  private Charset charsetIn;
  private Charset charsetOut;
  private Pattern attributePattern;
  private String keyTemplate;
  private String valueTemplate;
  private String keyType;
  private boolean sortAscending;

  @Override
  public String getName() {
    return name;
  }

  @Override
  public void configure(String name, Properties props, NameSpace ns) throws LDAPException {
    this.name = name;
    logger.info("name: " + name);
    logger = LogManager.getLogger(logger.getName() + ":" + name);
    attributeName = props.getProperty("attributeName");
    if (attributeName == null) {
      throw new IllegalArgumentException("No attribute name was given in the 'attributeName' property.");
    }
    logger.info("attributeName: " + attributeName);
    renameTo = props.getProperty("renameTo", null);
    logger.info("renameTo: " + renameTo);
    charsetIn = Charset.forName(props.getProperty("charsetIn", StandardCharsets.ISO_8859_1.name()));
    logger.info("charsetIn: " + charsetIn);
    charsetOut = Charset.forName(props.getProperty("charsetOut", StandardCharsets.ISO_8859_1.name()));
    logger.info("charsetOut: " + charsetOut);
    attributePattern = Pattern.compile(props.getProperty("attributeRegex", "^.*$"));
    logger.info("attributeRegex: " + attributePattern);
    keyTemplate = props.getProperty("keyTemplate", "{$0}");
    logger.info("keyTemplate: " + keyTemplate);
    valueTemplate = props.getProperty("valueTemplate", "{$0}");
    logger.info("valueTemplate: " + valueTemplate);
    keyType = props.getProperty("keyType", "string").toLowerCase();
    logger.info("keyType: " + keyType);
    final String sortDirection = props.getProperty("sortDirection", "ascending").toLowerCase();
    logger.info("sortDirection: " + sortDirection);
    if (sortDirection.equals("ascending")) {
      sortAscending = true;
    } else if (sortDirection.equals("descending")) {
      sortAscending = false;
    } else {
      throw new IllegalArgumentException("Unrecognized sort direction: '" + sortDirection + "'. Must be 'ascending' or 'descending'.");
    }
  }
  @Override
  public void postSearchEntry(PostSearchEntryInterceptorChain chain, Entry entry,
      DistinguishedName base, Int scope, Filter filter, ArrayList<Attribute> attributes,
      Bool typesOnly, LDAPSearchConstraints constraints) throws LDAPException {
    sortifier(entry);
    chain.nextPostSearchEntry(entry, base, scope, filter, attributes, typesOnly, constraints);
  }

  private void sortifier(Entry entry) {
    final LDAPEntry ldapEntry = entry.getEntry();
    final LDAPAttribute ldapAttribute = ldapEntry.getAttribute(attributeName);
    if (ldapAttribute == null) {
      logger.debug("Attribute " + attributeName + " not present; skipping sort");
      return;
    }
    final byte[][] byteValues = ldapAttribute.getByteValueArray();
    if (byteValues == null  ||  byteValues.length == 0) {
      logger.debug("Attribute " + attributeName + " had 0 values; skipping sort");
    } else {
      sortTheValues(ldapAttribute, byteValues);
    }
    if (renameTo != null) {
      logger.debug("Rename attribute '" + attributeName + "' to '" + renameTo + "'.");
      final LDAPAttribute existingAttributeWithThatName = ldapEntry.getAttribute(renameTo);
      if (existingAttributeWithThatName != null) {
        // LDAPentry behavior is to ignore the renaming if there is already
        // an attribute with that name. Not what we want.
        logger.debug("First, deleting existing attribute '" + renameTo + "'.");
        ldapEntry.getAttributeSet().remove(existingAttributeWithThatName);
      }
      entry.renameAttribute(attributeName, renameTo);
    }
  }

  private void sortTheValues(final LDAPAttribute ldapAttribute, final byte[][] byteValues) {
    logger.debug("Attribute " + attributeName + " has " + byteValues.length + " values to be sorted");
    final List<KV> list = new ArrayList<>();
    for (final byte[] oldBytes : byteValues) {
      ldapAttribute.removeValue(oldBytes);
      final String oldString = new String(oldBytes, charsetIn);
      parseAndAddToList(list, oldString);
    }
    @SuppressWarnings("unchecked")
    final Comparator<KV> comparator = sortAscending ? Comparator.naturalOrder() : Comparator.reverseOrder();
    Collections.sort(list, comparator);
    // we do this as a separate following loop to preserve the order of the values
    final Iterator<KV> it = list.iterator();
    while (it.hasNext()) {
      final KV item = it.next();
      final String value = item.getValue();
      final byte[] newBytes = value.getBytes(charsetOut);
      ldapAttribute.addValue(newBytes);
    }
  }

  private void parseAndAddToList(List<KV> list, String subject) {
    final String[] results = Util.matchAndReplace(attributePattern, subject, new String[] {keyTemplate, valueTemplate});
    final String key = results[0];
    final String value = results[1];
    Comparable<?> comparableKey;
    switch (keyType) {
      case "integer":
        comparableKey = Integer.parseInt(key);
        break;
      case "float":
        comparableKey = Float.parseFloat(key);
        break;
      case "boolean":
        comparableKey = Boolean.parseBoolean(key);
        break;
      case "string":
        comparableKey = key;
        break;
      default:
        throw new IllegalArgumentException("Unrecognized keyType property value: '" + keyType + "'.");
    }
    final KV kv = new KV(comparableKey, value);
    list.add(kv);
  }

  @SuppressWarnings("rawtypes")
  private static class KV implements Comparable {
    private final Comparable key;
    private final String value;
    KV(Comparable key, String value) {
      this.key = key;
      this.value = value;
    }
    @SuppressWarnings("unchecked")
    @Override
    public int compareTo(Object o) {
      final Comparable otherKey = ((KV)o).key;
      final int keyCompare = key.compareTo(otherKey);
      return keyCompare;
    }
    @Override
    public int hashCode() {
      return key.hashCode() + 17*value.hashCode();
    }
    @Override
    public boolean equals(Object obj) {
      if (! (obj instanceof KV)) {
        return false;
      }
      final KV other = (KV)obj;
      return (key.equals(other.key) && value.equals(other.value));
    }
    String getValue() {
      return value;
    }
  }
}
