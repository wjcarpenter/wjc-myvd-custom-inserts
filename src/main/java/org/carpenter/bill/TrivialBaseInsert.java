package org.carpenter.bill;

import java.util.ArrayList;
import java.util.Properties;
import com.novell.ldap.LDAPConstraints;
import com.novell.ldap.LDAPException;
import com.novell.ldap.LDAPModification;
import com.novell.ldap.LDAPSearchConstraints;
import net.sourceforge.myvd.chain.AddInterceptorChain;
import net.sourceforge.myvd.chain.BindInterceptorChain;
import net.sourceforge.myvd.chain.CompareInterceptorChain;
import net.sourceforge.myvd.chain.DeleteInterceptorChain;
import net.sourceforge.myvd.chain.ExetendedOperationInterceptorChain;
import net.sourceforge.myvd.chain.ModifyInterceptorChain;
import net.sourceforge.myvd.chain.PostSearchCompleteInterceptorChain;
import net.sourceforge.myvd.chain.PostSearchEntryInterceptorChain;
import net.sourceforge.myvd.chain.RenameInterceptorChain;
import net.sourceforge.myvd.chain.SearchInterceptorChain;
import net.sourceforge.myvd.core.NameSpace;
import net.sourceforge.myvd.inserts.Insert;
import net.sourceforge.myvd.types.Attribute;
import net.sourceforge.myvd.types.Bool;
import net.sourceforge.myvd.types.DistinguishedName;
import net.sourceforge.myvd.types.Entry;
import net.sourceforge.myvd.types.ExtendedOperation;
import net.sourceforge.myvd.types.Filter;
import net.sourceforge.myvd.types.Int;
import net.sourceforge.myvd.types.Password;
import net.sourceforge.myvd.types.Results;

/**
 * This class provides an Insert that does nothing but call the "next" items
 * in the applicable chains. There is no reason to use this Insert directly.
 * Instead, you can use it as a superclass for another Insert. This insert
 * provides all of the methods required by the Insert interface definition,
 * so the subclassing Insert need only override the methods that are interesting
 * to that subclass.
 *
 * This insert has no external dependencies.
 */
public class TrivialBaseInsert implements Insert {
  private String name;

  @Override
  public String getName() {  // probably never called
    return name;
  }

  @Override
  public void configure(String name, Properties props, NameSpace ns) throws LDAPException { // probably never called
    this.name = name;
  }

  @Override
  public void add(AddInterceptorChain chain, Entry entry, LDAPConstraints constraints) throws LDAPException {
    chain.nextAdd(entry, constraints);
  }

  @Override
  public void bind(BindInterceptorChain chain, DistinguishedName userDn, Password password, LDAPConstraints constraints) throws LDAPException {
    chain.nextBind(userDn, password, constraints);
  }

  @Override
  public void compare(CompareInterceptorChain chain, DistinguishedName base, Attribute attribute, LDAPConstraints constraints) throws LDAPException {
    chain.nextCompare(base, attribute, constraints);
  }

  @Override
  public void delete(DeleteInterceptorChain chain, DistinguishedName base, LDAPConstraints constraints) throws LDAPException {
    chain.nextDelete(base, constraints);
  }

  @Override
  public void extendedOperation(ExetendedOperationInterceptorChain chain, ExtendedOperation extOp, LDAPConstraints constraints) throws LDAPException {
    chain.nextExtendedOperations(extOp, constraints);
  }

  @Override
  public void modify(ModifyInterceptorChain chain, DistinguishedName base, ArrayList<LDAPModification> modifications, LDAPConstraints constraints) throws LDAPException {
    chain.nextModify(base, modifications, constraints);
  }

  @Override
  public void postSearchComplete(PostSearchCompleteInterceptorChain chain, DistinguishedName base, Int scope, Filter filter, ArrayList<Attribute> attributes, Bool typesOnly, LDAPSearchConstraints constraints) throws LDAPException {
    chain.nextPostSearchComplete(base, scope, filter, attributes, typesOnly, constraints);
  }

  @Override
  public void postSearchEntry(PostSearchEntryInterceptorChain chain, Entry entry, DistinguishedName base, Int scope, Filter filter, ArrayList<Attribute> attributes, Bool typesOnly, LDAPSearchConstraints constraints) throws LDAPException {
    chain.nextPostSearchEntry(entry, base, scope, filter, attributes, typesOnly, constraints);
  }

  @Override
  public void rename(RenameInterceptorChain chain, DistinguishedName dn, DistinguishedName newRdn, Bool deleteOldRdn, LDAPConstraints constraints) throws LDAPException {
    chain.nextRename(dn, newRdn, deleteOldRdn, constraints);
  }

  @Override
  public void rename(RenameInterceptorChain chain, DistinguishedName dn, DistinguishedName newRdn, DistinguishedName newParentDN, Bool deleteOldRdn, LDAPConstraints constraints) throws LDAPException {
    chain.nextRename(dn, newRdn, newParentDN, deleteOldRdn, constraints);
  }

  @Override
  public void search(SearchInterceptorChain chain, DistinguishedName base, Int scope, Filter filter, ArrayList<Attribute> attributes, Bool typesOnly, Results results, LDAPSearchConstraints constraints) throws LDAPException {
    chain.nextSearch(base, scope, filter, attributes, typesOnly, results, constraints);
  }

  @Override
  public void shutdown() {
  }
}
