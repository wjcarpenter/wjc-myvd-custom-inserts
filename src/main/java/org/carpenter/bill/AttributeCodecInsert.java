package org.carpenter.bill;

import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Base64;
import java.util.List;
import java.util.Properties;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import com.novell.ldap.LDAPAttribute;
import com.novell.ldap.LDAPEntry;
import com.novell.ldap.LDAPException;
import com.novell.ldap.LDAPSearchConstraints;
import net.sourceforge.myvd.chain.PostSearchEntryInterceptorChain;
import net.sourceforge.myvd.core.NameSpace;
import net.sourceforge.myvd.inserts.Insert;
import net.sourceforge.myvd.types.Attribute;
import net.sourceforge.myvd.types.Bool;
import net.sourceforge.myvd.types.DistinguishedName;
import net.sourceforge.myvd.types.Entry;
import net.sourceforge.myvd.types.Filter;
import net.sourceforge.myvd.types.Int;

/**
 * Sometimes, attribute values have the right information, but they're not
 * in the right format for consumption by an LDAP client. For example, maybe
 * the attribute values are base64 strings, and LDAP clients expect binary.
 *
 * This insert provides a few data conversion features and the optional opportunity
 * to rename the attribute being converted. For example, you might have an
 * attribute named "a64" that is in base64. After decoding base64, you can
 * rename the attribute to "a". Multiple conversions may be
 * configured. Some convert byte arrays to strings, and some convert strings
 * to byte arrays. When you chain multiple conversions together,
 * the framework will convert back and forth between byte arrays and strings,
 * as needed. The silent conversions are done via the converters "s>ISO-8859-1"
 * and "b>ISO-8859-1", which works well for 7- and 8-bit characters.
 *
 * attributeName (no default): The name of the attribute whose values will be
 * modified. This property is required in the configuration. However, if
 * no attribute of that name is present in LDAP entry, the insert silently
 * returns with no changes. Case-insensitive.
 *
 * renameTo (optional, no default): If configured, the attribute will be renamed
 * to this value. Case-insensitive. If there is already an attribute with this
 * name, it will be replaced.
 *
 * conversions (no default). A pipe-separated ("|") list of conversions to perform.
 * Extra whitespace around things in the list is ignored. Conversions are of the
 * form "s>CHARSET" (meaning: encode a string into a byte array) or "b>CHARSET"
 * (meaning: decode a byte array into a string). In either case, "CHARSET" is
 * the name of a character set recognized by Java. When using "s>CHARSET", the
 * selected character set must support encoding (most do). As a special, case there is
 * a pseudo-character set "base64" so that "b>base64" and "s>base64" may be used.
 *
 * This insert has no external dependencies.
 */
public class AttributeCodecInsert extends TrivialBaseInsert implements Insert {
  private Logger logger = LogManager.getLogger(AttributeCodecInsert.class);
  private String name;
  private String attributeName = null;
  private String renameTo = null;

  @Override
  public String getName() {
    return name;
  }

  @Override
  public void configure(String name, Properties props, NameSpace ns) throws LDAPException {
    this.name = name;
    logger.info("name: " + name);
    logger = LogManager.getLogger(logger.getName() + ":" + name);
    attributeName = props.getProperty("attributeName");
    if (attributeName == null) {
      throw new IllegalArgumentException("No attribute name was given in the 'attributeName' property.");
    }
    logger.info("attributeName: " + attributeName);
    renameTo = props.getProperty("renameTo", null);
    logger.info("renameTo: " + renameTo);
    final String converterNamesList = props.getProperty("conversions");
    logger.info("conversions: " + converterNamesList);
    converterList = AttributeCodecInsert.parseConverterList(converterNamesList);
    if (converterList == null) {
      throw new IllegalArgumentException("You must specify one or more conversions in the 'conversions' property.");
    }
  }
  @Override
  public void postSearchEntry(PostSearchEntryInterceptorChain chain, Entry entry,
      DistinguishedName base, Int scope, Filter filter, ArrayList<Attribute> attributes,
      Bool typesOnly, LDAPSearchConstraints constraints) throws LDAPException {
    codec(entry);
    chain.nextPostSearchEntry(entry, base, scope, filter, attributes, typesOnly, constraints);
  }

  private void codec(Entry entry) {
    final LDAPEntry ldapEntry = entry.getEntry();
    final LDAPAttribute ldapAttribute = ldapEntry.getAttribute(attributeName);
    if (ldapAttribute == null) {
      logger.debug("Attribute " + attributeName + " not present; skipping conversion");
      return;
    }
    final byte[][] byteValues = ldapAttribute.getByteValueArray();
    if (byteValues == null  ||  byteValues.length == 0) {
      logger.debug("Attribute " + attributeName + " had no values; skipping conversion");
    } else {
      convertTheValues(ldapAttribute, byteValues);
    }
    if (renameTo != null) {
      logger.debug("Renaming attribute '" + attributeName + "' to '" + renameTo + "'.");
      final LDAPAttribute existingAttributeWithThatName = ldapEntry.getAttribute(renameTo);
      if (existingAttributeWithThatName != null) {
        // LDAPentry behavior is to ignore the renaming if there is already
        // an attribute with that name. Not what we want.
        logger.debug("First, deleting existing attribute '" + renameTo + "'.");
        ldapEntry.getAttributeSet().remove(existingAttributeWithThatName);
      }
      entry.renameAttribute(attributeName, renameTo);
    }
  }

  private void convertTheValues(final LDAPAttribute ldapAttribute, final byte[][] byteValues) {
    logger.debug("Attribute " + attributeName + " has " + byteValues.length + " values to be converted");
    for (int ii = 0; ii < byteValues.length; ++ii) {
      final byte[] oldBytes = byteValues[ii];
      final byte[] newBytes = runTheConverters(oldBytes);
      ldapAttribute.removeValue(oldBytes);
      byteValues[ii] = newBytes;
    }
    // we do this as a separate following loop to preserve the order of the values
    for (final byte[] newBytes : byteValues) {
      ldapAttribute.addValue(newBytes);
    }
  }

  /**
   * Parses the pipe list of converter names, maps them to Converter objects,
   * and forms a list of the objects. Silent conversions to/from binary are
   * injected as needed.
   */
  private static List<Converter> parseConverterList(String converterListString) {
    if (converterListString == null) {
      return null;
    }
    // comma-separated list, but we tolerate spaces
    final String[] converterNames = converterListString.split("\\s*\\|\\s*");
    if (converterNames == null  ||  converterNames.length == 0) {
      return null;
    }
    final List<Converter> converters = new ArrayList<Converter>();
    Class<?> outputType = Converter.B;
    for (final String name : converterNames) {
      if (name.length() == 0) {
        continue;
      }
      final Converter converter = AttributeCodecInsert.findConverter(name);
      if (converter == null) {
        throw new IllegalArgumentException("There is no converter for '" + name + "'");
      }
      final Class<?> inputType = converter.getInputType();
      if (outputType.equals(Converter.B)  &&  inputType.equals(Converter.S)) {
        converters.add(AttributeCodecInsert.FROM_BINARY);
      } else if (outputType.equals(Converter.S)  &&  inputType.equals(Converter.B)) {
        converters.add(AttributeCodecInsert.TO_BINARY);
      }
      converters.add(converter);
      outputType = converter.getOutputType();
    }
    if (converters.size() == 0) {
      return null;
    }
    if (! outputType.equals(Converter.B)) { // the last configured converter output
      converters.add(AttributeCodecInsert.TO_BINARY);
    }
    return converters;
  }

  /**
   * Charset converters are identified by one of two forms:
   * binary-to-string converters as "b>NAME"; string-to-binary
   * converters as "s>NAME", where "NAME" is the name of a Charset
   * recognized by the JVM. Charset converters always have input
   * types and output types that are opposites (no byte-to-byte or
   * string-to-string). If the incoming rawName parameter doesn't
   * match the pattern, then null is returned. If the incoming
   * rawName parameter does match the pattern but the charset is
   * not found, an exception is thrown.
   */
  private static Converter findConverter(String rawName) {
    final String lowerRawName = rawName.toLowerCase();

    // special case for base64
    if ("b>base64".equals(lowerRawName)) {
      return new B_Base64();
    } else if ("s>base64".equals(lowerRawName)) {
      return new S_Base64();
    }

    final String charsetName;
    Class<?> inputType;
    if (lowerRawName.startsWith("s>")) {
      inputType = Converter.S;
      charsetName = rawName.substring(2);
    } else if (lowerRawName.startsWith("b>")) {
      inputType = Converter.B;
      charsetName = rawName.substring(2);
    } else {
      return null;
    }
    final Charset charset = Charset.forName(charsetName);
    if (inputType == Converter.S  &&  !charset.canEncode()) {
      throw new IllegalArgumentException("That charset " + charset.name() + " does not support encoding from string to bytes.");
    }
    final Converter converter = new CharsetConverter(charset, inputType);
    return converter;
  }

  private byte[] runTheConverters(byte[] bytes) {
    Object converterTarget = bytes;
    for (final Converter converter : converterList) {
      converterTarget = converter.convert(converterTarget);
    }
    return (byte[])converterTarget;
  }

  private List<Converter> converterList;
  private static Converter FROM_BINARY = AttributeCodecInsert.findConverter("b>" + StandardCharsets.ISO_8859_1.name());
  private static Converter TO_BINARY = AttributeCodecInsert.findConverter("s>" + StandardCharsets.ISO_8859_1.name());

  private static abstract class Converter {
    /**
     * A convenience constant for implementations to use in reporting input/output types.
     * This is the class for a byte array.
     */
    static final Class<byte[]> B = byte[].class;
    /**
     * A convenience constant for implementations to use in reporting input/output types.
     * This is the class for a String.
     */
    static final Class<String> S = String.class;
    /**
     * Gives the required input type for this converter class.
     */
    abstract Class<?> getInputType();
    /**
     * Gives the required output type for this converter class.
     */
    abstract Class<?> getOutputType();
    /**
     * This method does the actual conversion. The calling framework will guarantee that
     * the passed parameter is of the required type and will only accept a return value
     * of the required type. MUST BE THREADSAFE AND STATELESS. Instances might get reused.
     */
    abstract Object convert(Object input);
  }

  private static class CharsetConverter extends Converter {
    private final Charset charset;
    private final Class<?> inputType;
    CharsetConverter(Charset charset, Class<?> inputType) {
      this.charset = charset;
      this.inputType = inputType;
    }

    @Override
    public Class<?> getInputType() {
      return inputType;
    }
    @Override
    public Class<?> getOutputType() {
      return (inputType.equals(Converter.B) ? Converter.S : Converter.B);
    }
    @Override
    public Object convert(Object input) {
      if (inputType.equals(Converter.B)) {
        return new String((byte[])input, charset);
      } else {
        return ((String)input).getBytes(charset);
      }
    }
  }

  private static class S_Base64 extends Converter {
    @Override
    public Class<?> getInputType() {
      return Converter.S;
    }
    @Override
    public Class<?> getOutputType() {
      return Converter.B;
    }
    @Override
    public Object convert(Object input) {
      return Base64.getDecoder().decode((String)input);
    }
  }

  private static class B_Base64 extends Converter {
    @Override
    public Class<?> getInputType() {
      return Converter.B;
    }
    @Override
    public Class<?> getOutputType() {
      return Converter.S;
    }
    @Override
    public Object convert(Object input) {
      return Base64.getEncoder().encode((byte[])input);
    }
  }

}
