package org.carpenter.bill;

import java.util.Properties;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import com.novell.ldap.LDAPConstraints;
import com.novell.ldap.LDAPException;
import net.sourceforge.myvd.chain.BindInterceptorChain;
import net.sourceforge.myvd.core.NameSpace;
import net.sourceforge.myvd.inserts.Insert;
import net.sourceforge.myvd.types.DistinguishedName;
import net.sourceforge.myvd.types.Password;

/**
 * This class provides a bind() implementation that will accept any DN
 * and password. That's obviously not that useful if you need a real
 * LDAP bind, but it can be handy for experimentation when your real
 * LDAP bind logic is not ready to deploy. Always calls chainNext.
 *
 * This insert has no external dependencies.
 */
public class DummyBind extends TrivialBaseInsert implements Insert {
  private Logger logger = LogManager.getLogger(DummyBind.class);
  private String name;

  @Override
  public String getName() {
    return name;
  }

  @Override
  public void configure(String name, Properties props, NameSpace ns) throws LDAPException {
    this.name = name;
    logger.info("name: " + name);
    logger = LogManager.getLogger(logger.getName() + ":" + name);
  }

  @Override
  public void bind(BindInterceptorChain chain, DistinguishedName userDn, Password password, LDAPConstraints constraints) throws LDAPException {
    logger.info(this.getClass().getSimpleName() + ".bind() called for bind DN=" + userDn);
    chain.nextBind(userDn, password, constraints);
    return;
  }
}
